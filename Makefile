# Quick Makefile to compile easily the poster, uses latexmk
# which uses PDFLaTeX (pdflatex) and BibTeX (bibtex)
all: clean pdf evince clean

pdf:
	latexmk -gg -pdf Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en
	# pdflatex Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en.tex
	# pdflatex Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en.tex
	# bibtex Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en
	# pdflatex Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en.tex
	# pdflatex Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en.tex

png:
	convert Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en.pdf Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en.png

evince:
	-evince Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en.pdf >/dev/null 2>/dev/null &

clean:
	latexmk -c
	-rm -rfv *.fls *.fdb_latexmk *.ps *.dvi *.htoc *.tms *.tid *.lg *.log *.id[vx] *.vrb *.toc *.snm *.nav *.htmp *.aux *.tmp *.out *.haux *.hidx *.bbl *.blg *.brf *.lof *.ilg *.ind *.meta *.fdb_latexmk *.fls *.synctex.gz*

bst:
	tex naereen.dbj
	mv -vf naereen.log /tmp/

# Download my script from https://bitbucket.org/lbesson/bin/src/master/PDFCompress
compress:
	PDFCompress Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en.pdf
